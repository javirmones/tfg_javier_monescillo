from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

from usuarios import views as user_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('registrarse/', user_views.register, name='registrarse'),
    path('perfil/', user_views.perfil, name='perfil'),
    path('login/', auth_views.LoginView.as_view(template_name='usuarios/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='usuarios/logout.html') , name='logout'),
    path('', include('repositorio.urls'))

]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
