from django.apps import AppConfig


class RepositorioConfig(AppConfig):
    name = 'repositorio'

    def ready(self):
        import usuarios.signals
