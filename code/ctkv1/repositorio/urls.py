from django.urls import path
from . import views
from .views import (
    ModelosListView,
    ModelosCreateView,
    ModelosUpdateView,
    ModelosDetailView,
    ModelosDeleteView

    )

urlpatterns = [
        path('',  views.inicio , name='repositorio-inicio'),
        path('ayuda/', views.ayuda, name='repositorio-ayuda'),
        path('predecir/', views.predecir, name='repositorio-predecir'),
        path('predecir/resultado/', views.resultado, name='repositorio-resultado'),
        path('modelos/', ModelosListView.as_view(), name='repositorio-listar_modelos'),
        path('modelos/<int:pk>', ModelosDetailView.as_view(), name='modelokeras-detail'),
        path('modelos/<int:pk>/borrar/',ModelosDeleteView.as_view() , name='modelo-borrar'),
        path('post/<int:pk>/actualizar/', ModelosUpdateView.as_view() , name='modelo-actualizar'),
        path('modelos/subir_modelo/', ModelosCreateView.as_view() , name='modelo-nuevo')


]
