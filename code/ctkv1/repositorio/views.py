from django.shortcuts import render, redirect, get_object_or_404
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils.encoding import codecs

from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView )

from .formularios import ModeloKerasForm, PredecirForm
from .models import ModeloKeras
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

import sys
import pickle
import h5py
import os


MAX_NUM_WORDS = 30000
MAX_SEQUENCE_LENGTH = 1000
PATH = "./media/predict/"
FILE_LIMIT = 8

def inicio(request):
    context = {}
    return render(request, 'repositorio/home.html', context)

@login_required
def ayuda(request):
    return render(request, 'repositorio/ayuda.html', {})

def resultado(request):
    context = {}
    return render(request, 'repositorio/resultado.html',context)

class ModelosListView(ListView):
    model = ModeloKeras
    template_name = 'repositorio/listar_modelos.html'
    context_object_name = 'modelos'
    ordering = ['-fecha']
    #<app>/<model>_<viewtype>.html

class ModelosDetailView(DetailView):
    model = ModeloKeras
    #queryset = ModeloKeras.objects.all()


class ModelosCreateView(LoginRequiredMixin, CreateView):
    model = ModeloKeras
    fields = ['nombre_modelo','modelo','contenido']

    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form)

class ModelosUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = ModeloKeras
    fields = ['nombre_modelo','modelo','contenido']

    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form)

    def test_func(self):
        ModeloKeras = self.get_object()
        if self.request.user == ModeloKeras.autor:
            return True
        return False

class ModelosDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = ModeloKeras
    success_url = '/modelos/'

    def test_func(self):
        ModeloKeras = self.get_object()
        if self.request.user == ModeloKeras.autor:
            return True
        return False

def listar_modelos(request):
    modelos = Modelos.objects.all()
    return render(request, 'blog/listar_modelos.html', {'modelos':modelos})

@login_required
def predecir(request):
    form = {}
    context ={}
    aux_dict = {}
    aux_dict_class = {}
    texts = []
    fs = FileSystemStorage()

    if request.method =='POST':
        form = PredecirForm(request.POST, request.FILES)
        if form.is_valid():
            limit = 0
            # Escribir los archivos subidos en media/predict #
            for count, x in enumerate(request.FILES.getlist('archivos_predecir')):
                def procesar(file):
                    limit = 0
                    with open(PATH + "file_" + str(count) +".txt", 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)
                            limit += 1
                    return limit
                limit += procesar(x)

            if limit <= FILE_LIMIT:
                # Obtener modelo #
                modelo_path=form.cleaned_data.get('modelo')
                modelo = str(modelo_path.modelo)
                name_tokenizer = "tokenizer_" + str(modelo_path.nombre_modelo)+".pickle"
                final_path = "./media/" + modelo

                # Escribir el tokenizer subido
                for f in request.FILES.getlist('tokenizer'):
                    fs.save(name_tokenizer,f)

                modelo = load_model(final_path)
                # loading tokenizer
                with open("./media/" + name_tokenizer, 'rb') as handle:
                    tokenizer = pickle.load(handle)

                # Leer dichos archivos en la lista text #
                texts = files_to_list(PATH)
                seq_test = to_sequences(texts, tokenizer)
                seq = reshape_seq(seq_test)
                predictions = predict(seq, modelo)

                procesar = form.cleaned_data.get('procesar')
                to_process = procesar.split(",")

                create_dict(aux_dict, predictions, 'Doc')
                create_dict(aux_dict_class, to_process, 'Clas')

                context['predictions'] = aux_dict
                context['procesar'] = aux_dict_class

                # Eliminar archivos .pickle y carpeta predictions
                remove_files(PATH, ".txt")
                remove_files("./media/", ".pickle")

                return render(request, 'repositorio/resultado.html', context)
            else:
                return render(request, 'repositorio/predecir.html', {'form':form })

    else:
        form = PredecirForm()
    return render(request, 'repositorio/predecir.html', {'form':form })

def to_sequences(data_test, tokenizer):
    sequences_test = tokenizer.texts_to_sequences(data_test)
    pad_sequences_test = pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH)
    return pad_sequences_test

def reshape_seq(sequences):
    seq_reshaped = []
    for seq in sequences:
        seq_reshaped.append(seq.reshape(1,MAX_SEQUENCE_LENGTH))
    return seq_reshaped

def predict(seq, model):
    predictions = []
    for text_pred in seq:
        predictions.append(model.predict(text_pred))

    return predictions

def files_to_list(path):
    texts = []
    for fname in sorted(os.listdir(path)):
        fpath = os.path.join(path, fname)
        args = {} if sys.version_info <( 3, ) else {'encoding': 'utf-8', 'errors': 'ignore'}
        with open(fpath, **args) as file:
            t = file.read()
            i = t.find('\n\n')
            if 0 < i:
                t = t[i:]
            texts.append(t)
            file.close()
    return texts

def remove_files(path, ext):
    lista_archivos = [ f for f in os.listdir(path) if f.endswith(ext) ]
    for f in lista_archivos:
        os.remove(path+f)

def create_dict(dicts, lists, name):
    for count, x in enumerate(lists):
        dicts[name+str(count)] = x
