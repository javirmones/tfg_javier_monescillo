from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from ckeditor_uploader.fields import RichTextUploadingField



class ModeloKeras(models.Model):
    nombre_modelo = models.CharField(max_length=100)
    modelo = models.FileField(upload_to='modelos/')
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField(default=timezone.now)
    contenido = RichTextUploadingField()


    def __str__(self):
        return self.nombre_modelo

    def get_absolute_url(self):
        return reverse('modelokeras-detail', kwargs={'pk': self.pk})
