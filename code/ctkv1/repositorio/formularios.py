from django import forms

from .models import ModeloKeras

class ModeloKerasForm(forms.ModelForm):
    nombre_modelo = forms.CharField(label='', widget =forms.TextInput(attrs={"placeholder":"Nombre del modelo"}))
    modelo = forms.FileField(widget=forms.ClearableFileInput(attrs={'accept':'.h5'}))

    class Meta:
        model = ModeloKeras
        fields = ['nombre_modelo','modelo','contenido']

class PredecirForm(forms.Form):
    modelo = forms.ModelChoiceField(queryset=ModeloKeras.objects.all())
    archivos_predecir = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    tokenizer = forms.FileField(widget=forms.ClearableFileInput(attrs={'accept':'.pickle'}))
    procesar = forms.CharField()
