from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .formulario import UsuarioRegistradoForm
from repositorio.models import ModeloKeras as Modelo
from django.contrib.auth.models import User

def register(request):
    if request.method == 'POST':
        form = UsuarioRegistradoForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'¡Cuenta creada correctamente para {username}! Ahora puede entrar correctamente al sistema.')
            return redirect('repositorio-inicio')
    else:
        form = UsuarioRegistradoForm()
    return render(request, 'usuarios/registrarse.html', {'form':form})

@login_required
def perfil(request):
    mod = Modelo.objects.all()
    context = {
        'modelo':mod
    }
    return render(request, 'usuarios/perfil.html',context)
