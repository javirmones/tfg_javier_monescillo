from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget
from .models import Perfil

class UsuarioRegistradoForm(UserCreationForm):
    email = forms.EmailField()
    captcha = ReCaptchaField(widget=ReCaptchaWidget())

    class Meta:
        model = User
        fields = ['username','email', 'password1', 'password2']

class UsuarioActualizarForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username','email')

class PerfilActualizarForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ('imagen',)
