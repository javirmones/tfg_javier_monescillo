
from nltk.corpus import stopwords
from nltk.stem import LancasterStemmer, WordNetLemmatizer

import numpy as np
import pandas as pd

import unicodedata
import random
import string
import os
import sys
import re

TEXT_DATA_DIR = 'DATASET_INDRA'
TEXT_DATA_DIR_NEWS = 'DATASET_NEWS'
TEXT_DATA_INIT_PATH ='DATASET_INDRA/Inicial/'
TEXT_DATA_INTER_PATH ='DATASET_INDRA/Intermedio/'
categories = ['Inicial', 'Intermedio', 'Final']
texts = [] #lista de muestras de texto
labels_index = {} # diccionario que mapea una label name a un id
labels = [] # lista de labels id
l_aux = []
l_list = []
texts_news = []
labels_index_news = {}
labels_news = []

class Preproceso():
    ''' Clase relativa a la fase de preproceso '''
    def __init__(self):
        print("Iniciando Preproceso")
        self.procesar_csv_indra()
        self.procesar_csv_noticias()


    def procesar_csv_indra(self):
        self.recorrer_directorios(TEXT_DATA_DIR, categories, labels, labels_index, texts)
        cont_fin, cont_inter, cont_ini = self.contar_elementos_clase()
        self.grafico(cont_ini, cont_inter, cont_fin)
        #self.borrar_desbalanceados_inicial()
        #cont_fin1, cont_inter1, cont_ini1 = self.contar_elementos_clase()
        #print("Eliminando elementos iniciales")
        #self.grafico(cont_ini1, cont_inter1, cont_fin1)
        #self.borrar_desbalanceados_inter(cont_inter1)
        #cont_fin2, cont_inter2, cont_ini2 = self.contar_elementos_clase()
        #print("Eliminando elementos intermedios")
        #self.grafico(cont_ini2, cont_inter2, cont_fin2)

    def procesar_csv_noticias(self):
        self.recorrer_directorios(TEXT_DATA_DIR_NEWS, categories_news, labels_news, labels_index_news, texts_news)
        cont_at, cont_gr, cont_win, cont_med, cont_christ = self.contar_elementos_noticias()
        self.grafico_noticias(cont_at, cont_gr, cont_win, cont_med, cont_christ)



    def recorrer_directorios(self, path_dataset, categories, labels, labels_index, texts):
        ''' Recorrer_directorios '''
        for name in sorted(os.listdir(path_dataset)):
                path = os.path.join(path_dataset, name)
                if os.path.isdir(path):
                    label_id = len(labels_index)
                    labels_index[name] = label_id

                    for fname in sorted(os.listdir(path)):
                        fpath = os.path.join(path, fname)
                        args = {} if sys.version_info <( 3, ) else {'encoding': 'latin-1'}
                        with open(fpath, **args) as file:
                            t = file.read()
                            i = t.find('\n\n')
                            if 0 < i:
                                t = t[i:]
                            texts.append(t)
                            file.close()
                        labels.append(label_id)

        print('Encontrados %s textos' % len(texts))

    def contar_elementos_clase(self):
        ''' Contar elementos de los directorios '''
        cont_fin = 0
        cont_inter = 0
        cont_ini = 0

        for t in labels:
            if t == 0:
                l_aux.append("Final")
                cont_fin += 1
            elif t == 1:
                l_aux.append("Inicial")
                cont_ini += 1
            elif t == 2:
                l_aux.append("Intermedio")
                cont_inter += 1

        return cont_fin, cont_inter, cont_ini

    def contar_elementos_noticias(self):
        cont_at = 0
        cont_gr = 0
        cont_win = 0
        cont_med = 0
        cont_christ = 0

        for t in labels_news:
            if t == 0:
                l_list.append("Atheism")
                cont_at+=1
            elif t == 1:
                l_list.append("Graphics")
                cont_gr+=1
            elif t == 2:
                l_list.append("Windows")
                cont_win+=1
            elif t == 3:
                l_list.append("Med")
                cont_med+=1
            elif t == 4:
                l_list.append("Christian")
                cont_christ+=1

        return cont_at, cont_gr, cont_win, cont_med, cont_christ

    def borrar_desbalanceados_inicial(self):
        ''' Eliminar elementos sobrantes del directorio inicial '''
        files = os.listdir(TEXT_DATA_INIT_PATH)
        tot_borrar = len(files) - 1500
        self.borrar_elementos(files, TEXT_DATA_INIT_PATH, tot_borrar)

    def borrar_desbalanceados_inter(self, cont_inter):
        ''' Eliminar elementos sobrantes del directorio intermedio '''
        files_inter = os.listdir(TEXT_DATA_INTER_PATH)
        tot_borr_inter = cont_inter - 1450
        self.borrar_elementos(files_inter, TEXT_DATA_INTER_PATH, tot_borr_inter )

    def borrar_elementos(self, files, path, tot_borrar):
        ''' Eliminar elementos de un directorio '''
        aleatorios = random.sample(range(0,tot_borrar),tot_borrar)
        for name in aleatorios:
            if files[name] in files:
                if files[name].endswith(".txt"):
                    os.remove(path+"%s" % files[name])


    def grafico(self, cont_ini, cont_inter, cont_fin):
        ''' Generar gráfico del balance actual '''
        dic_plots = {'Iniciales':[cont_ini],'Intermedias':[cont_inter], 'Finales':[cont_fin]}
        dft  = pd.DataFrame(dic_plots)
        print(dft)
        #dft.plot(kind='bar', title="Documentos por clase")

    def grafico_noticias(self, cont_at, cont_gr, cont_win, cont_med, cont_christ):
        dic_plots = {'Ateos':[cont_at],'Graficos':[cont_gr], 'Windows':[cont_win], 'Med':[cont_med], 'Cristianos':[cont_christ]}
        dft  = pd.DataFrame(dic_plots)
        print(dft)


class Limpieza():
    ''' Clase relativa a la fase de limpieza '''

    def __init__(self):
        print("Fase de limpieza")
        self.limpiar_indra()
        self.limpiar_noticias()

    def limpiar_noticias(self):
        words = self.limpiar_textos(texts_news)
        words_joined = self.unir(words)
        self.to_dataframe(TEXT_DATA_DIR_NEWS, "noticias.csv", words_joined, labels_news, l_list)

    def limpiar_indra(self):
        words = self.limpiar_textos(texts)
        words_joined = self.unir(words)
        self.to_dataframe(TEXT_DATA_DIR, "facturas.csv", words_joined, labels, l_aux)


    def eliminar_numeros(self, words):
        ''' Eliminar numeros '''
        exp = re.compile('[0-9]+')
        words = [exp.sub('',w) for w in words]
        return words

    def eliminar_puntuacion(self, words):
        ''' Eliminar puntuación '''
        re_print = re.compile('[^%s]' % re.escape(string.printable))
        result = [re_print.sub('', w) for w in words]
        return result

    def eliminar_stopwords(self, words):
        ''' Eliminar stopwords '''
        stop_words = set(stopwords.words('spanish'))
        words = [w for w in words if not w in stop_words]
        return words

    def to_lowercase(self, words):
        ''' Convertir a minúsculas'''
        words = [word.lower() for word in words]
        return words

    def eliminar_no_ascii(self, words):
        """Remove non-ASCII characters from list of tokenized words"""
        new_words = []
        for word in words:
            new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
            new_words.append(new_word)
        return new_words

    def limpiar(self, words):
        ''' Metodo general '''
        words = re.split(r'\W+', words)

        words = self.eliminar_numeros(words)
        words = self.eliminar_puntuacion(words)
        words = self.eliminar_no_ascii(words)
        #words = self.eliminar_stopwords(words)
        words = self.to_lowercase(words)
        return words

    def limpiar_textos(self, textos):
        words = []
        for t in textos:
            words.append(self.limpiar(t))
        return words


    def unir(self, words):
        ''' Unir las palabrass '''
        words_joined = []

        for w in range(0,len(words)):
            words_joined.append(' '.join(words[w]))
        return words_joined

    def to_dataframe(self, path, nombre_csv, words_joined, labels, l_aux):
        ''' To dataframe '''
        test = {'Texto':words_joined, 'Clase':l_aux, 'Id_clase':labels}
        df = pd.DataFrame(test)
        df.head()
        csv = os.path.join(path, nombre_csv)
        df.to_csv(csv, index=0)
        print("generado dataset ",nombre_csv)


if __name__ == '__main__':
    preproceso = Preproceso()
    limpieza = Limpieza()
