#!/usr/bin/env python
# coding: utf-8

# ## Entrenamiento de redes neuronales Conv y LSTM

# In[13]:


from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

from tensorflow.keras.utils import to_categorical
from tensorflow.keras.layers import Dense, Input, GlobalMaxPooling1D, Dropout, Flatten
from tensorflow.keras.layers import Conv1D, MaxPooling1D, Embedding, Layer, LSTM, SpatialDropout1D
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.models import Sequential
from tensorflow.keras.initializers import Constant
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from time import time
from tensorflow.python.client import device_lib
from time import time

import matplotlib.pyplot as plt
import tensorflow.keras.layers as layers
import tensorflow.keras.backend as tensorflow_backend
import tensorflow as tf
import numpy as np
import pandas as pd

import pprint
import os
import sys
import h5py
import pickle


# ### Configuración GPU

# In[14]:


config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True),log_device_placement=True)
session = tf.Session(config=config)
tensorflow_backend.set_session(session)

print(device_lib.list_local_devices())


# ## Rutas y constantes

# In[33]:


DIR_DATASET = '../data/noticias.csv'
NOMBRE_COLUMNAS = ['Texto','Clase']
TEST_SIZE = 0.10
RANDOM_STATE = 42
NAME_TOKENIZER = "../data/tokenizer_news.pickle"


# ### Lectura de dataset

# In[21]:


def read_dataset(path, nombre_cols ):
    dataset = pd.read_csv(DIR_DATASET)
    id_labels = list(dataset['Id_clase'])
    X = dataset[nombre_cols]
    return dataset, id_labels, X


# In[22]:


dataset, id_labels, X = read_dataset(DIR_DATASET, NOMBRE_COLUMNAS)


# In[23]:


dataset.tail()


# ### Separación entrenamiento y test

# In[24]:


def separar_train_test(X, id_labels, test_size, random_state):
    return train_test_split( X, id_labels, test_size=test_size, random_state=random_state)


# In[25]:


X_train, X_test, y_train, y_test = separar_train_test(X, id_labels, TEST_SIZE, RANDOM_STATE)
train_l = list(X_train['Texto'])
test_l = list(X_test['Texto'])


# In[11]:


train_l[0]


# ### Carga word embeddings

# In[26]:


GLOVE_EMBEDDING_PATH = '../data/glove.6B.100d.txt'

def get_coefs(word,*arr): return word, np.asarray(arr, dtype='float32')

def load_embeddings(embed_dir):
    embedding_index = dict(get_coefs(*o.strip().split(" ")) for o in tqdm(open(embed_dir)))
    return embedding_index

glove = load_embeddings(GLOVE_EMBEDDING_PATH)


# ### Carga alternativa de word embeddings

# In[ ]:


path_emb = 'glove.840B.300d.pkl'

t = time()
with open(path_emb, 'rb') as fp:
    glove = pickle.load(fp)
print(time()-t)


# ### Configuración de ambos modelos

# In[30]:


MAX_SEQUENCE_LENGTH = 1000
MAX_NUM_WORDS = 30000
EMBEDDING_DIM = 100
FILTERS = 128
KERNEL_SIZE = 5
ACTIVATION = 'softmax'
ACTIVATION_CONV_LAYERS = 'relu'
POOL_SIZE = 5
BATCH_SIZE = 128
EPOCHS = 15
CALLBACK = EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)
LOSS = 'categorical_crossentropy'
OPTIMIZER = 'adam'
METRICS = 'acc'
NUMBER_OF_CLASSES = 17


# ### Representación matemática de los textos

# In[31]:


tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
tokenizer.fit_on_texts(train_l)
tokenizer.fit_on_texts(test_l)
sequences_train = tokenizer.texts_to_sequences(train_l)
sequences_test = tokenizer.texts_to_sequences(test_l)
word_index = tokenizer.word_index
num_words = min(MAX_NUM_WORDS, len(word_index)) + 1


# In[34]:


def save_tokenizer(path_name, tokenizer):
    with open(path_name, 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
save_tokenizer(NAME_TOKENIZER, tokenizer)


# In[37]:


data_train = pad_sequences(sequences_train, maxlen=MAX_SEQUENCE_LENGTH)
data_test = pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH)


# In[38]:


labels_train = to_categorical(np.asarray(y_train))
labels_test = to_categorical(np.asarray(y_test))


# In[39]:


print('Dimensiones del tensor de los datos de entrenamiento:', data_train.shape)
print('Dimensiones del tensor de los datos de prueba:', data_test.shape)
print('Dimensiones del tensor de las etiquetas de entrenamiento: ', labels_train.shape)
print('Dimensiones del tensor de las etiquetas de prueba:', labels_test.shape)


# ### Creación de la capa y la matriz de  word embeddings 

# In[40]:


def create_embedding_matrix(num_words, max_words, dim, word_index, glove):
    embedding_matrix = np.zeros((num_words, dim))
    for word, i in word_index.items():
        if i > max_words:
            continue
        embedding_vector = glove.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
    return embedding_matrix

embedding_matrix = create_embedding_matrix(num_words, MAX_NUM_WORDS, EMBEDDING_DIM, word_index, glove)


# In[41]:


def create_embedding_layer(num_words, dim, embedding_matrix, max_sequence_length):
    embedding_layer = Embedding(input_dim=num_words,
                                output_dim=dim,
                                embeddings_initializer=Constant(embedding_matrix),
                                input_length=MAX_SEQUENCE_LENGTH, trainable=False)
    return embedding_layer

embedding_layer = create_embedding_layer(num_words, EMBEDDING_DIM, embedding_matrix, MAX_SEQUENCE_LENGTH)


# ### Entrenamiento modelo convolucional

# In[18]:


def train_conv_model(max_sequence_length, filters, kernel_size, activation, loss, custom_optimizer, metrics,
                     activation_conv_layers, pool_size, batch_size, epochs, dropout_rate, cb, number_of_classes):
    sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')

    embedded_seq= embedding_layer(sequence_input)

    layer = Conv1D(filters=filters, kernel_size=kernel_size, activation=activation_conv_layers)(embedded_seq)
    layer = MaxPooling1D(pool_size=pool_size)(layer)
    layer = Dropout(rate=dropout_rate)(layer)
    layer = Conv1D(filters=filters, kernel_size=kernel_size, activation=activation_conv_layers)(layer)
    layer = GlobalMaxPooling1D()(layer)
    layer = Flatten()(layer)
    layer = Dense(units=filters, activation=activation_conv_layers)(layer)
    preds = Dense(units=number_of_classes, activation=activation)(layer)

    model = Model(sequence_input, preds)
    model.compile(loss=loss, optimizer=custom_optimizer, metrics=[metrics])

    historia=model.fit(data_train, labels_train, batch_size=batch_size, epochs=epochs, validation_data=(data_test, labels_test),
                       callbacks=[cb])
    return model, historia


# In[19]:


model, history = train_conv_model(max_sequence_length= MAX_SEQUENCE_LENGTH, filters=FILTERS,
                                         kernel_size=KERNEL_SIZE, activation=ACTIVATION,
                                         loss=LOSS, custom_optimizer=OPTIMIZER, metrics=METRICS,
                                         activation_conv_layers=ACTIVATION_CONV_LAYERS,
                                         pool_size=POOL_SIZE, batch_size=BATCH_SIZE,
                                         epochs=EPOCHS, cb=CALLBACK,
                                         number_of_classes=NUMBER_OF_CLASSES)


# ### Entrenamiento modelo LSTM

# In[2]:


def train_lstm_model(max_sequence_length, num_words, dim, spatial_dropout, units_lstm,
                     dropout_lstm, rr_dropout_lstm, loss, activation_dense, optimizer, metrics,
                     batch_size, epochs, cb, n_clases):
    model_lstm = Sequential()
    model_lstm.add(Embedding(num_words, dim, input_length=MAX_SEQUENCE_LENGTH))
    model_lstm.add(SpatialDropout1D(spatial_dropout))
    model_lstm.add(LSTM(units=units_lstm, dropout=dropout_lstm, recurrent_dropout=rr_dropout_lstm))
    model_lstm.add(Dense(n_clases, activation=activation_dense))
    model_lstm.compile(loss=loss, optimizer=optimizer, metrics=[metrics])
    historia = model_lstm.fit(data_train, labels_train, batch_size=batch_size, epochs=epochs, validation_data=(data_test, labels_test),
                       callbacks=[cb])
    
    return  model_lstm, historia

   


# In[3]:


model_lstm, history = train_lstm_model(max_sequence_length= MAX_SEQUENCE_LENGTH, num_words=num_words,
                                        dim=100, spatial_dropout=0.2, units_lstm=100, dropout_lstm=0.2,
                                        rr_dropout_lstm=0.2, loss=LOSS, activation_dense = "softmax", 
                                        optimizer=OPTIMIZER, metrics=METRICS, batch_size = 100,
                                        epochs=EPOCHS_LSTM, cb=CALLBACK, n_clases=NUMBER_OF_CLASSES
                                        )


# ### Acc vs Loss

# In[45]:


def plot_history(titulo, historia, val_1, val_2):
    plt.title(titulo)
    plt.plot(historia.history[val_1], label='train')
    plt.plot(historia.history[val_2], label='test')
    plt.legend()
    plt.show()

plot_history('Loss', history, 'loss','val_loss')
plot_history('Acc', history, 'acc', 'val_acc')


# ### Evaluar un modelo

# In[ ]:


def evaluate_model(model, data_test, labels_test):
    accr = model.evaluate(data_test,labels_test)
    print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))
    
evaluate_model(model, data_test, labels_test)


# ### Guardar un modelo

# In[50]:


def save_model(model, name):
    model.save(name + '.h5')
    
save_model(model_lstm,"lstm_news")


# ### Predicciones sobre un modelo

# In[46]:


def to_sequences(data_test, tokenizer):
    sequences_test = tokenizer.texts_to_sequences(data_test)
    data_test_seq = pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH)
    return data_test_seq

def model_predict(y_seq, data_test_seq, model):
    seq_list = []
    predictions = []
    
    for text_seq in range(0,len(y_seq)):
        seq_list.append(data_test_seq[text_seq].reshape((1,MAX_SEQUENCE_LENGTH)))

    for text_pred in seq_list:   
        predictions.append(model.predict(text_pred).argmax())
        
    return predictions

t = time()
predictions = model_predict(y_test, data_test, model_lstm)
print(time()-t)


# In[25]:


target_names_indra = ["Final", "Inicial", "Intermedio"]


# In[48]:


target_names_news = ["comp.graphics",
"comp.os.ms-windows.misc",
"comp.sys.ibm.pc.hardware",
"comp.sys.mac.hardware",
"comp.windows.x",
"misc.forsale",
"rec.autos",
"rec.motorcycles",
"rec.sport.baseball",
"rec.sport.hockey",
"sci.crypt",
"sci.electronics",
"sci.med",
"sci.space",
"soc.religion.christian",
"talk.politics.guns",
"talk.politics.mideast"]
y_test


# ### Crear informe de predicciones

# In[49]:


def check_class_report(y_real, y_preds, classes_names):
    print(classification_report(y_real, y_preds, target_names=classes_names))
    
check_class_report(y_test, predictions, target_names)

