## Trabajo Fin de Grado - Clasificador automático de documentos con técnicas de procesamiento del lenguaje natural

Aunque actualmente sean tendencia y estén siendo muy utilizadas, las redes neuronales no tuvieron
un fuerte impacto cuando fueron inicialmente propuestas. Su estado de implantación actual se debe
principalmente al gran beneficio que aportan en multitud de problemas.

Su gran crecimiento en los últimos tiempos se ha debido a la conectividad y propagación de
las redes en torno a lo que hoy en día se conoce como Internet, que produce una cantidad de datos
masiva. Pero sobre todo se debe al desarrollo de la tecnología, y a la existencia de hardware con gran
capacidad de computo.

Esta combinación de avances informáticos, junto con la gran cantidad de datos disponibles, o que
se puedan minar, ha conseguido convertir a las redes neuronales en uno de los algoritmos de deep
learning más populares hasta la fecha.
Este proyecto en concreto, forma parte de un convenio entre la universidad y la compañía Indra,
empresa que en la actualidad está desarrollando una línea de investigación dentro del campo del
deep learning.

De alguna forma, el problema de Indra es relativo a la documentación, necesitan automatizar la
gestión documental relativa a facturas del departamento Imaging. Este departamento se encarga de
resolver problemas de una forma clásica, ya bien sea utilizando técnicas OCR en documentos estructurados,
o búsqueda de patrones estáticos en elementos que siempre son iguales en su identificación.
El problema planteado por la empresa Indra Sistemas versa sobre la necesidad de construir un
clasicador multiclase general para clasificar automáticamente documentos, utilizando técnicas de
machine learning, deep learning y natural lenguaje processing.

El siguiente documento, pretende exponer un sistema que incluye una serie de técnicas, modelos
y resultados que resuelven el problema proporcionado por Indra Sistemas, e integran el sistema en
una aplicación web escrita con el framework Django.

## ClaTek - Clasificador de Textos en keras

ClaTek se trata de una herramienta web que pretende resolver problemas de clasificación multiclase, nace a raíz del Forte entre la ESI y la compañía Indra.

![alt text](/resources/clatek.jpeg)

## Estructura del proyecto
El proyecto se estructura principalmente en los siguientes directorios:

* **code**: Todos los elementos relativos al código desarrollado.
* **resources**: Recursos necesarios para este repositorio.
* **slides**: Transparencias que van a ser utilizadas en la presentación.
* **tfg**: Documento que integra la memoria del Trabajo Fin de Grado.

## Organización del proyecto

El proyecto se organiza en diversos módulos, siguiendo así una arquitectura de capas:

![alt text](/resources/web.PNG)

Donde cada una de las capas, tiene unos determinados submódulos o subprocesos:

![alt text](/resources/cap2.PNG)

## Datasets utilizados

* Dataset de Indra: No disponible.
* Dataset _20newsgroup_: Disponible [aquí](http://qwone.com/~jason/20Newsgroups/).

## Word embeddings utilizados

  * GloVe (_Global Vectors for word representation_): Disponible [aquí](https://nlp.stanford.edu/projects/glove/).
  * Embeddings de Kaggle: Disponible [aquí](https://www.kaggle.com/rtatman/pretrained-word-vectors-for-spanish).

## Dependencias
  * Python >= v3.7
  * Tensorflow o Tensorflow_gpu >= v1.13
  * Jinja2 >= v2.10
  * Django >= v2.2.5
  * Pandas >= v0.24
  * Conda enviroment <= v1.9.6


## Participantes del proyecto
  * Autor: Javier Monescillo Buitrón
  * Tutor académico: José Jesús Castro Sánchez
  * Cotutor académico: Luis Ripoll Morales
