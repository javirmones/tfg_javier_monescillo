\BOOKMARK [0][-]{idx_resumen.0}{Resumen}{}% 1
\BOOKMARK [0][-]{idx_abstract.0}{Abstract}{}% 2
\BOOKMARK [0][-]{idx_agrad.0}{Agradecimientos}{}% 3
\BOOKMARK [0][-]{idx_toc.0}{\315ndice general}{}% 4
\BOOKMARK [0][-]{section*.7}{\315ndice de figuras}{}% 5
\BOOKMARK [0][-]{section*.9}{\315ndice de tablas}{}% 6
\BOOKMARK [0][-]{section*.11}{\315ndice de listados}{}% 7
\BOOKMARK [0][-]{chapter.1}{1 Introducci\363n}{}% 8
\BOOKMARK [1][-]{section.1.1}{1.1 Contexto}{chapter.1}% 9
\BOOKMARK [1][-]{section.1.2}{1.2 Problema}{chapter.1}% 10
\BOOKMARK [1][-]{section.1.3}{1.3 Estado del arte}{chapter.1}% 11
\BOOKMARK [2][-]{subsection.1.3.1}{1.3.1 Historia de la clasificaci\363n}{section.1.3}% 12
\BOOKMARK [2][-]{subsection.1.3.2}{1.3.2 Representaci\363n matem\341tica}{section.1.3}% 13
\BOOKMARK [2][-]{subsection.1.3.3}{1.3.3 Modelos estad\355sticos cl\341sicos}{section.1.3}% 14
\BOOKMARK [2][-]{subsection.1.3.4}{1.3.4 Aprendizaje autom\341tico}{section.1.3}% 15
\BOOKMARK [2][-]{subsection.1.3.5}{1.3.5 Aprendizaje profundo}{section.1.3}% 16
\BOOKMARK [2][-]{subsection.1.3.6}{1.3.6 Redes neuronales artificiales}{section.1.3}% 17
\BOOKMARK [2][-]{subsection.1.3.7}{1.3.7 Redes neuronales convolucionales}{section.1.3}% 18
\BOOKMARK [2][-]{subsection.1.3.8}{1.3.8 Redes neuronales recurrentes}{section.1.3}% 19
\BOOKMARK [2][-]{subsection.1.3.9}{1.3.9 Otras arquitecturas}{section.1.3}% 20
\BOOKMARK [0][-]{chapter.2}{2 Objetivo}{}% 21
\BOOKMARK [1][-]{section.2.1}{2.1 Objetivo general}{chapter.2}% 22
\BOOKMARK [1][-]{section.2.2}{2.2 Subobjetivos}{chapter.2}% 23
\BOOKMARK [0][-]{chapter.3}{3 Metodolog\355a}{}% 24
\BOOKMARK [1][-]{section.3.1}{3.1 Metodolog\355a de desarrollo}{chapter.3}% 25
\BOOKMARK [1][-]{section.3.2}{3.2 Fases de desarrollo}{chapter.3}% 26
\BOOKMARK [2][-]{subsection.3.2.1}{3.2.1 Pre-Game}{section.3.2}% 27
\BOOKMARK [2][-]{subsection.3.2.2}{3.2.2 Game}{section.3.2}% 28
\BOOKMARK [2][-]{subsection.3.2.3}{3.2.3 Post-Game}{section.3.2}% 29
\BOOKMARK [1][-]{section.3.3}{3.3 Metodolog\355a de trabajo}{chapter.3}% 30
\BOOKMARK [0][-]{chapter.4}{4 Resultados}{}% 31
\BOOKMARK [1][-]{section.4.1}{4.1 Sprint 1}{chapter.4}% 32
\BOOKMARK [2][-]{subsection.4.1.1}{4.1.1 T1. Estudio e investigaci\363n del estado del arte en el problema}{section.4.1}% 33
\BOOKMARK [2][-]{subsection.4.1.2}{4.1.2 T2. Dise\361o del clasificador autom\341tico y sistema general}{section.4.1}% 34
\BOOKMARK [2][-]{subsection.4.1.3}{4.1.3 T3. Definici\363n de los datasets y las clases de entrenamiento}{section.4.1}% 35
\BOOKMARK [2][-]{subsection.4.1.4}{4.1.4 T4. Construir m\363dulo de preproceso para los documentos}{section.4.1}% 36
\BOOKMARK [2][-]{subsection.4.1.5}{4.1.5 T5. Construir m\363dulo de limpieza para los documentos preprocesados}{section.4.1}% 37
\BOOKMARK [2][-]{subsection.4.1.6}{4.1.6 T6. Construir los datasets para la clasificaci\363n}{section.4.1}% 38
\BOOKMARK [2][-]{subsection.4.1.7}{4.1.7 T7. Construir m\363dulo para la divisi\363n de los dataset en conjunto de test y entrenamiento}{section.4.1}% 39
\BOOKMARK [1][-]{section.4.2}{4.2 Sprint 2}{chapter.4}% 40
\BOOKMARK [2][-]{subsection.4.2.1}{4.2.1 T8. Construir la representaci\363n matem\341tica de los dataset basada en word embeddings}{section.4.2}% 41
\BOOKMARK [2][-]{subsection.4.2.2}{4.2.2 T9. Construir modelo basado en redes neuronales convolucionales}{section.4.2}% 42
\BOOKMARK [2][-]{subsection.4.2.3}{4.2.3 T10. Construir modelo basado en redes neuronales LSTM}{section.4.2}% 43
\BOOKMARK [2][-]{subsection.4.2.4}{4.2.4 T11. Construir m\363dulo para configurar y recuperar modelos}{section.4.2}% 44
\BOOKMARK [2][-]{subsection.4.2.5}{4.2.5 T12. Construir m\363dulo para la predicci\363n de modelos}{section.4.2}% 45
\BOOKMARK [2][-]{subsection.4.2.6}{4.2.6 T13. Construir m\363dulo para estudio y comparativa de modelos}{section.4.2}% 46
\BOOKMARK [1][-]{section.4.3}{4.3 Sprint 3}{chapter.4}% 47
\BOOKMARK [2][-]{subsection.4.3.1}{4.3.1 T14. Construir una aplicaci\363n web y sistema de base de datos}{section.4.3}% 48
\BOOKMARK [2][-]{subsection.4.3.2}{4.3.2 T15. Construir la gesti\363n de usuarios de la aplicaci\363n web}{section.4.3}% 49
\BOOKMARK [2][-]{subsection.4.3.3}{4.3.3 T16. Construir la gesti\363n de modelos del servicio web}{section.4.3}% 50
\BOOKMARK [2][-]{subsection.4.3.4}{4.3.4 T17. Integrar al backend el m\363dulo de predicci\363n de la aplicaci\363n web}{section.4.3}% 51
\BOOKMARK [2][-]{subsection.4.3.5}{4.3.5 T18. Crear manual de usuario para la utilizaci\363n de la aplicaci\363n web}{section.4.3}% 52
\BOOKMARK [0][-]{chapter.5}{5 Conclusiones}{}% 53
\BOOKMARK [1][-]{section.5.1}{5.1 Resumen}{chapter.5}% 54
\BOOKMARK [1][-]{section.5.2}{5.2 Objetivos cumplidos}{chapter.5}% 55
\BOOKMARK [1][-]{section.5.3}{5.3 Competencias}{chapter.5}% 56
\BOOKMARK [1][-]{section.5.4}{5.4 Mejoras en trabajo futuro}{chapter.5}% 57
\BOOKMARK [1][-]{section.5.5}{5.5 Valoraci\363n cr\355tica}{chapter.5}% 58
\BOOKMARK [0][-]{appendix.Alph1}{A Facturas}{}% 59
\BOOKMARK [0][-]{appendix.Alph2}{B Problema Indra}{}% 60
\BOOKMARK [0][-]{section*.126}{Bibliograf\355a}{}% 61
