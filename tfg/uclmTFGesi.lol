\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.1}C\IeC {\'o}digo relativo al procesamiento de texto}{32}{lstlisting.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.2}C\IeC {\'o}digo relativo a la limpieza del texto}{33}{lstlisting.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.3}C\IeC {\'o}digo relativo al inicio del preproceso y limpieza }{34}{lstlisting.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.4}C\IeC {\'o}digo uni\IeC {\'o}n del texto}{34}{lstlisting.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.5}C\IeC {\'o}digo relativo a la recuperaci\IeC {\'o}n de datasets}{34}{lstlisting.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.6}C\IeC {\'o}digo lectura dataset}{35}{lstlisting.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.7}C\IeC {\'o}digo relativo a la separaci\IeC {\'o}n del dataset}{35}{lstlisting.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.8}C\IeC {\'o}digo carga de word embeddings}{36}{lstlisting.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.9}C\IeC {\'o}digo carga r\IeC {\'a}pida de word embeddings}{37}{lstlisting.4.9}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.10}C\IeC {\'o}digo Tokenizer Keras}{37}{lstlisting.4.10}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.11}C\IeC {\'o}digo tratamiento de secuencias}{37}{lstlisting.4.11}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.12}C\IeC {\'o}digo recuperaci\IeC {\'o}n Tokenizer }{38}{lstlisting.4.12}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.13}C\IeC {\'o}digo creaci\IeC {\'o}n matriz de word embeddings}{38}{lstlisting.4.13}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.14}C\IeC {\'o}digo creaci\IeC {\'o}n capa de word embeddings}{39}{lstlisting.4.14}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.15}C\IeC {\'o}digo entrenamiento modelo convolucional}{39}{lstlisting.4.15}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.16}C\IeC {\'o}digo configuraci\IeC {\'o}n Tensorflow}{40}{lstlisting.4.16}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.17}C\IeC {\'o}digo llamada modelo convolucional}{40}{lstlisting.4.17}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.18}C\IeC {\'o}digo entrenamiento modelo LSTM}{41}{lstlisting.4.18}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.19}C\IeC {\'o}digo llamada modelo LSTM}{42}{lstlisting.4.19}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.20}C\IeC {\'o}digo generaci\IeC {\'o}n de gr\IeC {\'a}ficos}{42}{lstlisting.4.20}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.21}Configuraci\IeC {\'o}n com\IeC {\'u}n de hiperpar\IeC {\'a}metros}{43}{lstlisting.4.21}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.22}C\IeC {\'o}digo para el paso de texto a secuencia}{45}{lstlisting.4.22}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.23}C\IeC {\'o}digo para realizar predicciones}{45}{lstlisting.4.23}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.24}C\IeC {\'o}digo para evaluar un modelo}{46}{lstlisting.4.24}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.25}Estructura de archivos y directorios de la aplicaci\IeC {\'o}n web}{52}{lstlisting.4.25}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.26}Estructura de rutas del m\IeC {\'o}dulo principal}{53}{lstlisting.4.26}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.27}C\IeC {\'o}digo configuraci\IeC {\'o}n m\IeC {\'o}dulo usuarios}{55}{lstlisting.4.27}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.28}C\IeC {\'o}digo relativo a modelos en el m\IeC {\'o}dulo de usuarios}{55}{lstlisting.4.28}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.29}C\IeC {\'o}digo relativo a se\IeC {\~n}ales m\IeC {\'o}dulo de usuarios}{56}{lstlisting.4.29}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.30}C\IeC {\'o}digo relativo a los formularios m\IeC {\'o}dulo usuarios}{56}{lstlisting.4.30}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.31}C\IeC {\'o}digo relativo a las vistas m\IeC {\'o}dulo de usuarios}{57}{lstlisting.4.31}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.32}C\IeC {\'o}digo configuraci\IeC {\'o}n m\IeC {\'o}dulo repositorio}{59}{lstlisting.4.32}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.33}C\IeC {\'o}digo relativo a las rutas m\IeC {\'o}dulo repositorio}{59}{lstlisting.4.33}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.34}C\IeC {\'o}digo relativo a modelos m\IeC {\'o}dulo repositorio}{60}{lstlisting.4.34}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.35}C\IeC {\'o}digo relativo a formulario m\IeC {\'o}dulo repositorio}{60}{lstlisting.4.35}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.36}C\IeC {\'o}digo relativo a las vistas m\IeC {\'o}dulo repositorio}{61}{lstlisting.4.36}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.37}C\IeC {\'o}digo relativo al formulario de predicci\IeC {\'o}n}{64}{lstlisting.4.37}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.38}C\IeC {\'o}digo relativo a las vistas m\IeC {\'o}dulo de predicci\IeC {\'o}n}{70}{lstlisting.4.38}
