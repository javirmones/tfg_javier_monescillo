\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {spanish}{}
\babel@toc {spanish}{}
\babel@toc {spanish}{}
\babel@toc {english}{}
\babel@toc {spanish}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xvii}}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\'{I}ndice de tablas}{\es@scroman {xix}}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxi}}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Contexto}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Problema}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Estado del arte}{4}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Historia de la clasificaci\IeC {\'o}n}{4}{subsection.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Representaci\IeC {\'o}n matem\IeC {\'a}tica}{4}{subsection.1.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Funciones de distancia}{5}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bag of Words}{6}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Modelos estad\IeC {\'\i }sticos cl\IeC {\'a}sicos}{6}{subsection.1.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Modelos probabil\IeC {\'\i }sticos}{7}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Algoritmo del vecino m\IeC {\'a}s proximo}{7}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.4}Aprendizaje autom\IeC {\'a}tico}{7}{subsection.1.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Regresi\IeC {\'o}n lineal}{8}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.5}Aprendizaje profundo}{9}{subsection.1.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Word embeddings}{9}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.6}Redes neuronales artificiales}{10}{subsection.1.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Perceptr\IeC {\'o}n simple}{10}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Perceptr\IeC {\'o}n multicapa}{11}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.7}Redes neuronales convolucionales}{12}{subsection.1.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.8}Redes neuronales recurrentes}{14}{subsection.1.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Redes neuronales LSTM}{14}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.9}Otras arquitecturas}{16}{subsection.1.3.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{ELMO}{16}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{BERT}{16}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Objetivo}{17}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Objetivo general}{17}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Subobjetivos}{17}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Metodolog\IeC {\'\i }a}{19}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Metodolog\IeC {\'\i }a de desarrollo}{19}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Fases de desarrollo}{20}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Pre-Game}{20}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Herramientas hardware}{21}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Herramientas software}{21}{section*.36}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Control de versiones}{21}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Product Backlog}{22}{section*.39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Game}{23}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Post-Game}{23}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Metodolog\IeC {\'\i }a de trabajo}{24}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Resultados}{27}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Sprint 1}{27}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}T1. Estudio e investigaci\IeC {\'o}n del estado del arte en el problema}{28}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}T2. Dise\IeC {\~n}o del clasificador autom\IeC {\'a}tico y sistema general}{28}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}T3. Definici\IeC {\'o}n de los datasets y las clases de entrenamiento}{30}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}T4. Construir m\IeC {\'o}dulo de preproceso para los documentos}{31}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}T5. Construir m\IeC {\'o}dulo de limpieza para los documentos preprocesados}{33}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.6}T6. Construir los datasets para la clasificaci\IeC {\'o}n}{34}{subsection.4.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.7}T7. Construir m\IeC {\'o}dulo para la divisi\IeC {\'o}n de los dataset en conjunto de test y entrenamiento}{35}{subsection.4.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Sprint 2}{36}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}T8. Construir la representaci\IeC {\'o}n matem\IeC {\'a}tica de los dataset basada en word embeddings}{36}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}T9. Construir modelo basado en redes neuronales convolucionales}{38}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}T10. Construir modelo basado en redes neuronales LSTM}{41}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}T11. Construir m\IeC {\'o}dulo para configurar y recuperar modelos}{43}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.5}T12. Construir m\IeC {\'o}dulo para la predicci\IeC {\'o}n de modelos}{45}{subsection.4.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.6}T13. Construir m\IeC {\'o}dulo para estudio y comparativa de modelos}{46}{subsection.4.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Resultados}{46}{section*.74}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Sprint 3}{51}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}T14. Construir una aplicaci\IeC {\'o}n web y sistema de base de datos}{51}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}T15. Construir la gesti\IeC {\'o}n de usuarios de la aplicaci\IeC {\'o}n web}{55}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Modelo}{55}{section*.90}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Se\IeC {\~n}ales}{56}{section*.92}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Formularios}{56}{section*.94}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Vistas}{57}{section*.96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Plantillas}{58}{section*.98}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}T16. Construir la gesti\IeC {\'o}n de modelos del servicio web}{59}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Modelos}{60}{section*.103}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Formularios}{60}{section*.105}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Vistas}{61}{section*.107}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Plantillas}{62}{section*.109}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}T17. Integrar al backend el m\IeC {\'o}dulo de predicci\IeC {\'o}n de la aplicaci\IeC {\'o}n web}{63}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Formulario}{64}{section*.113}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Vistas}{64}{section*.115}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Plantillas}{65}{section*.117}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.5}T18. Crear manual de usuario para la utilizaci\IeC {\'o}n de la aplicaci\IeC {\'o}n web}{66}{subsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusiones}{71}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Resumen}{71}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Objetivos cumplidos}{71}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Competencias}{72}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Mejoras en trabajo futuro}{73}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Valoraci\IeC {\'o}n cr\IeC {\'\i }tica}{74}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Facturas}{75}{appendix.Alph1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Problema Indra}{77}{appendix.Alph2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{85}{section*.126}
